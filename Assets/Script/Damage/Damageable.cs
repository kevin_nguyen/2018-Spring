﻿using UnityEngine;
using System;
using UnityEngine.Events;

/* Health component 
 * Usage: place on any components that can take damage required 
 */
public class Damageable : MonoBehaviour
{
    [Serializable]
    public class HealthEvent : UnityEvent<Damageable>
    {
    }

    [Serializable]
    public class DamageEvent : UnityEvent<Damager, Damageable>
    {
    }
    public void DestroySelfOnHit(Damager damage, Damageable health)
    {
        Destroy(health.gameObject);
    }
    
    public static void LogDamageEvent(Damager damage, Damageable health)
    {
        Debug.Log("Damage:" + health.gameObject.name +
                  " takes " + damage.damage +
                  " from " + damage.gameObject);
    }

    [Serializable]
    public class HealEvent : UnityEvent<int, Damageable>
    {
    }

    public int maxHealth = 5;
    int currentHealth = 5;
    public bool invulnrable = false;
    public bool debugLog;

    [Header("Events")]
    public HealthEvent onHealthSet;
    public HealEvent onGainHealth;
    public DamageEvent onTakeDamage;
    public DamageEvent onDie;

    public int CurrentHealth()
    {
        return currentHealth;
    }

    public void Start()
    {
        if (debugLog)
        {
           onTakeDamage.AddListener(LogDamageEvent);
        }
    }

    void OnEnable()
    {
        resetHealth();
        onHealthSet.Invoke(this);
        DisableInvulnerability();
    }

    public void EnableInvulnerability()
    {
        invulnrable = true;
    }

    public void DisableInvulnerability()
    {
        invulnrable = false;
    }

    public void TakeDamage(Damager damager)
    {
        if ((invulnrable) || currentHealth <= 0)
            return;

        if (!invulnrable)
        {
            currentHealth -= damager.damage;
            onHealthSet.Invoke(this);
        }

        onTakeDamage.Invoke(damager, this);

        if (currentHealth <= 0)
        {
            onDie.Invoke(damager, this);
        }
    }

    public void GainHealth(int amount)
    {
        currentHealth += amount;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }


        onHealthSet.Invoke(this);
        onGainHealth.Invoke(amount, this);
    }

    public void SetHealth(int amount)
    {
        currentHealth = amount; //NOTE setHealth can go over max

        onHealthSet.Invoke(this);
    }

    public void resetHealth()
    {
        currentHealth = maxHealth;
    }
}