﻿using UnityEngine;

public class DamageCollider2D : Damager
{
    void OnCollisionEnter2D(Collision2D col)
    {
        DamageObject(col.gameObject);
    }
}