﻿using UnityEngine;

namespace Assets.Script.Damage
{
    public class DestroyOnDeath : MonoBehaviour
    {
        private void Start()
        {
            var damageable = GetComponent<Damageable>();
            if(damageable != null)
            {
                damageable.onDie.AddListener(OnDie);
            }
        }

        private void OnDie(Damager damager, Damageable damageable)
        {
            Destroy(gameObject);
        }
    }
}
