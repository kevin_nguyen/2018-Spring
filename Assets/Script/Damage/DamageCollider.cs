﻿using UnityEngine;

namespace Script.Damage
{
    public class DamageCollider : Damager
    {
        private void OnCollisionEnter(Collision other)
        {
            DamageObject(other.gameObject);
            if (destoyAfterHit)
            {
                Destroy(gameObject);
            }
        }
    }
}