﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour
{
    [Serializable]
    public class DamagableEvent : UnityEvent<Damager, Damageable>
    {
    }

    [Serializable]
    public class NonDamagableEvent : UnityEvent<Damager>
    {
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
    
    public int damage = 1;
    public bool canDamage = true;
    public bool disableAfterHit = false;
    public bool destoyAfterHit;
    public DamagableEvent OnDamageableHit;
    public NonDamagableEvent OnNonDamageableHit;


    public virtual void EnableDamage()
    {
        canDamage = true;
    }

    public virtual void DisableDamage()
    {
        canDamage = false;
    }

    public virtual void DamageObject(GameObject obj)
    {
        if (!canDamage)
            return;

        Damageable damageable = obj.GetComponent<Damageable>();
        if (damageable)
        {
            OnDamageableHit.Invoke(this, damageable);
            damageable.TakeDamage(this);
            if (disableAfterHit)
                DisableDamage();
            if(destoyAfterHit)
                Destroy(this);
        }
        else
        {
            OnNonDamageableHit.Invoke(this);
        }
    }
}