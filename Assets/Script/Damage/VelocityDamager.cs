﻿using UnityEngine;

public class VelocityDamager : Damager
{
    private int _baseDamage;
    public AnimationCurve damageMultipler;

    private void Start()
    {
        _baseDamage = damage;
    }

    public override void DamageObject(GameObject obj)
    {
        var body = obj.GetComponent<Rigidbody>();

        float speed = body.velocity.magnitude;
        float multiplier = damageMultipler.Evaluate(speed);
        
        damage = (int)(_baseDamage * multiplier);
    }
}
