﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatusState
{
    void StateStart(StatusController sm, IStatusState lastState);
    IStatusState Update(StatusController sm);
    //void FixedUpdate(Status status);
}
