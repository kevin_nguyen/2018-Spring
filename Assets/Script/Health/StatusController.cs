﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

public class StatusController : MonoBehaviour, IStatusController
{
	public NeutralState neutral { get; private set; }
	public DeadState dead { get; private set; }
	public HitState hit { get; private set; }
	public HitInvulnerableState invulnerable { get; private set; }
	public RespawnInvulnerableState respawn { get; private set; }
	public int ID;

	public Status status;
	
	public IStatusState currentState { get; private set; }

	void Start () {
		neutral = new NeutralState();
		dead = new DeadState();
		hit = new HitState();
		invulnerable = new HitInvulnerableState();
		respawn = new RespawnInvulnerableState();
		
		status = new Status();
		
		currentState = neutral;
	}
	
	void Update ()
	{
		var healthState = currentState.Update(this);
		if (healthState != currentState)
		{
			healthState.StateStart(this, currentState);
			currentState = healthState;
		}
	}
	
	public void Resurrect()
	{
		status.dead = false;
	}

	public IStatusState GetCurrentState()
	{
		return currentState;
	}

	public void ReceiveDamage(int player, int value)
	{
		Debug.Log("Hit: whoHitMe="+player+", damageReceived="+value);
		status.whoHitMe = player;
		status.damageReceived = value;
		Debug.Log("HP:"+status.health);
	}
	
	public IEnumerator Cooldown(System.Action condition, float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		condition();
	}
}
