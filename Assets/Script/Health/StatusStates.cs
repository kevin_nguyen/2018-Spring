﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

//figure out who hit me
public class NeutralState : IStatusState {
    public void StateStart(StatusController sm, IStatusState lastState){}

    public IStatusState Update(StatusController sm)
    {
        if (sm.status.hit)
            return sm.hit;
        return sm.neutral;
    }
}

//resolve who hit me
public class HitState : IStatusState
{
    public void StateStart(StatusController sm, IStatusState lastState)
    {        
        //**should probably move stuff like this to start without any problems; see if it works first though
        try{
            sm.status.health -= sm.status.damageReceived;
            Debug.Log("Player "+sm.ID+" hit by "+sm.status.whoHitMe);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Damage received was a null value: ignoring damage taken\n" + e);
        }   
    }

    public IStatusState Update(StatusController sm)
    {

        if (sm.status.health <= 0)
            return sm.dead;
        
        return sm.invulnerable;
    }
}

public class HitInvulnerableState : IStatusState
{
    public void StateStart(StatusController sm, IStatusState lastState)
    {
        sm.status.invulnerable = true;
        //**
        sm.StartCoroutine(sm.Cooldown(() => sm.status.invulnerable = false, sm.status.hitInvulnerable));
    }
    
    public IStatusState Update(StatusController sm)
    {


        if (!sm.status.invulnerable)
            return sm.neutral;
        
        return sm.invulnerable;
    }
}

//reward who last hit me
public class DeadState : IStatusState {
    public void StateStart(StatusController sm, IStatusState lastState)
    {
        sm.status.dead = true;
        GameManager.instance.AddScore(sm.ID, 2000);
        Debug.Log("I am dead.");
    }

    public IStatusState Update(StatusController sm)
    {
        if (!sm.status.dead)
            return sm.respawn;
        
        return sm.dead;
    }
}

public class RespawnInvulnerableState : IStatusState
{
    public void StateStart(StatusController sm, IStatusState lastState)
    {
        GameManager.instance.Respawn(sm.ID);
        sm.status.invulnerable = true;
        //**
        sm.status.health = 100;
        sm.StartCoroutine(sm.Cooldown(() => sm.status.invulnerable = false, sm.status.respawnInvulnerable));
    }

    public IStatusState Update(StatusController sm)
    {

        
        if (!sm.status.invulnerable)
            return sm.neutral;
        
        return sm.respawn;
    }
}

