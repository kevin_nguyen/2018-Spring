﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class Status
{
	public int health;

	public float respawnInvulnerable = 3F;
	public float hitInvulnerable = 1F;

	public bool hit; //send these as eventData to StatusStateMachine instead
	public bool dead; //.
	public bool invulnerable; //..
	public int damageReceived = 0; //...
	public int whoHitMe = 0; //how do I use events please save me

	public Status()
	{
		health = 100;
		dead = false;
		invulnerable = false;
		hit = false;
	}
}
