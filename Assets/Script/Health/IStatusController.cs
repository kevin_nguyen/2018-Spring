﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatusController
{
    void ReceiveDamage(int player, int value = 100);
    void Resurrect();
    IStatusState GetCurrentState();
}
