﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    //ID:Score
    public Dictionary<int, int> score;
    //ID:PlayerObj
    public Dictionary<int, GameObject> players;
    public GameObject[] respawnNodes;
    public int max = 25000;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance)
        {
            Destroy(gameObject);
        }

    }
    //just to make sure that players are initiated before gathering arrays
    private void Start()
    {
        respawnNodes = GameObject.FindGameObjectsWithTag("Respawn");
        GameObject[] playerArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < playerArray.Length; i++)
        {
            players.Add(i, playerArray[i]);
            score.Add(i, 0);
            playerArray[i].gameObject.GetComponent<StatusController>().ID = i;
        }
    }

    public void AddScore(int player, int value)
    {
        score[player] += value;
    }

    public void Respawn(int player)
    {
            int node = Random.Range(0, 100) % respawnNodes.Length;
            players[player].transform.position = respawnNodes[node].transform.position;
    }
}
