﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
	public Weapon weapon;
	public float cooldownTime;
	public bool hasCoolDown;

	public void Start()
	{
		weapon.parentObject = gameObject;
	}
	
	public GameObject CmdFire()
	{
		if (!weapon)
		{
			Debug.LogError("No weapon set Obj.Name="+ name);
			return null;	
		}

		if (!hasCoolDown)
		{
			
			hasCoolDown = true;
			StartCoroutine(WaitFor(()=>{hasCoolDown = false;},cooldownTime));
			return weapon.CmdFire();
		}

		return null;
	}

	public GameObject CmdFire(bool shouldFire)
	{
		if (shouldFire)
		{
			return CmdFire();
		}

		return null;
	}

	public IEnumerator WaitFor(System.Action afterWait, float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		afterWait();
	}
}
