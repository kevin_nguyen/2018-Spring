﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Script
{
    public class AsteroidController : MonoBehaviour
    {
        public List<WeaponController> asteroidSpawners;
        public float maxDistance;
        public float frequency;
        public Transform player;
        private bool hasCoolDown;

        public void Start()
        {
            asteroidSpawners = new List<WeaponController>(GetComponentsInChildren<WeaponController>());
        }

        private void Update()
        {
            if (!hasCoolDown)
            {
                SpawnRandomAsteroid();
                hasCoolDown = true;
                StartCoroutine(WaitFor(()=>{hasCoolDown = false;},frequency));
            }
        }
        public IEnumerator WaitFor(System.Action afterWait, float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            afterWait();
        }

        public void SpawnRandomAsteroid()
        {
            GameObject bullet = asteroidSpawners[Random.Range(0, asteroidSpawners.Count)].CmdFire();
            DistanceDestroyer distanceConstraint = bullet.AddComponent( typeof(DistanceDestroyer)) as DistanceDestroyer;
            distanceConstraint.maxDistance = maxDistance;
            distanceConstraint.target = player;
            distanceConstraint.enabled = true;
        }
    }
}