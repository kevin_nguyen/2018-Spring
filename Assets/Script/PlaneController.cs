﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class PlaneController : MonoBehaviour
{
    private Rigidbody rb;

    public float pitchMod,
        maxAngularVelocity,
        maxDepenetrationVelocity,
        rollMod,
        yawMod,

        thrustMod;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = maxAngularVelocity;
        rb.maxDepenetrationVelocity = maxDepenetrationVelocity;
    }

    public void Pitch(float amount)
    {
        rb.transform.Rotate(amount * pitchMod, 0, 0);
    }

    public void Roll(float amount)
    {
        rb.transform.Rotate(0, 0, amount * rollMod);
    }

    public void Yaw(float amount)
    {
        rb.transform.Rotate(0, 0, amount * yawMod);
    }

    public void Thrust(float amount)
    {
        rb.velocity = transform.forward * (amount * thrustMod);
    }

    public void InputUpdate(float pitch, float roll, float yaw, float thrust)
    {
        rb.AddRelativeTorque(pitch * pitchMod, yaw * yawMod, roll * rollMod);
        rb.AddRelativeForce(Vector3.forward * thrust * thrustMod);
    }
}