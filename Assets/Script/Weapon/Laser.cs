﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
/*
public class Laser : Weapon
{
    public int damage = 100;
    public float range = 100F;
    private RaycastHit hit;
    private LineRenderer lineRenderer;
    private PlayerPlaneController controller;
    private GameObject lightHit;
    private Light light;
    public Color startColor;
    public Color endColor;

    private void Awake()
    {
        lineRenderer = this.transform.gameObject.AddComponent<LineRenderer>();
        controller = this.GetComponentInParent<PlayerPlaneController>();
        lightHit = new GameObject("Light");
        light = lightHit.AddComponent<Light>();
        lightHit.transform.parent = this.gameObject.transform;
    }

    private void Start()
    {
        lineRenderer.enabled = false;
        lineRenderer.SetColors(startColor, endColor);
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        light.enabled = false;
        light.color = endColor;
        light.intensity = 10F;
        light.range = 4F;
    }

    private void Update()
    {
        if (!controller.input.fire)
        {
            lineRenderer.enabled = false;
            light.enabled = false;
        }
    }

    public override void Fire()
    {
        lineRenderer.enabled = true;
        light.enabled = true;
        if (Physics.Raycast(this.transform.position, this.transform.forward.normalized, out hit, range))
        {
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, hit.point);
            if (hit.transform.CompareTag("Player"))
            {
                hit.transform.GetComponent<IStatusController>()
                    .ReceiveDamage(this.gameObject.transform.parent.GetComponent<StatusController>().ID, damage);
                this.gameObject.transform.parent.GetComponent<StatusController>().status.hit = true;
            }
        }
        else
        {
            light.enabled = false;
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, this.transform.position + (this.transform.forward.normalized * range));
        }
    }
}*/