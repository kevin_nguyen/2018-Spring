﻿using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Networking;
using UnityEngine.Timeline;
[CreateAssetMenu(fileName = "Projectile", menuName = "Weapon/Projectile")]
public class ProjectileWeapon : Weapon
{
    public GameObject projectile;
    public float velocity;
    
    [Command]
    public override GameObject CmdFire()
    {
        GameObject obj = Instantiate(projectile);
        
        
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        if (!rb)
        {
            Debug.Log("Projectile Fired does not contain Rigidbody");
            return null;
        }
        rb.position = parentObject.transform.position;
        rb.rotation = parentObject.transform.rotation;
        rb.velocity = parentObject.transform.parent.GetComponent<Rigidbody>().velocity + (parentObject.transform.forward * velocity);
        return obj;
    }
}