﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : ScriptableObject
{
     [HideInInspector]public GameObject parentObject;
     
     public abstract GameObject CmdFire();
}