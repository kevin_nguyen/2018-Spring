﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class DistanceDestroyer : MonoBehaviour
{
	public Transform target;
	public float maxDistance;
	void Start()
	{
		if (!target)
		{
			Debug.LogError("DistanceDestroyer has No target");
			enabled = false;
		}
	}

	void Update()
	{
		if (Vector3.Distance(target.transform.position,transform.position) > maxDistance)
		{
			Destroy(gameObject);
		}
	}
}
