﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlaneController))]
[RequireComponent(typeof(WeaponController))]
public class PlayerController : MonoBehaviour
{
    private PlaneController plane;
    public WeaponController weapon;

    public PlayerInput input;
    public PlayerInput.InputType inputType;

    void Start()
    {
        plane = GetComponent<PlaneController>();
    }


    void Update()
    {

        input.Update();
        input.type = inputType;
        //input.debugLog();

        plane.InputUpdate(input.pitch, input.roll, input.yaw, input.thrust);
        weapon.CmdFire(input.fire);
    }
}