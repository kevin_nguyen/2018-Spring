﻿using UnityEngine;
using UnityEngine.Networking;

public class TagIfLocal : NetworkBehaviour
{
    public string Tag;

    void Start()
    {
        if(isLocalPlayer)
        {
            this.tag = Tag;
        }
    }
}
