﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

[CreateAssetMenu(fileName = "PlayerInput", menuName = "InputSettings")]
public class PlayerInput : ScriptableObject
{
    public enum InputType
    {
        MouseAndKeyboard,
        Controller,
        TouchScreen
    }

    public InputType type;

    //Values
    [HideInInspector] public float thrust;
    [HideInInspector] public float pitch;
    [HideInInspector] public float roll;
    [HideInInspector] public float yaw;
    [HideInInspector] public bool fire;
    [HideInInspector] public bool pickup;

    //keyboard
    [Header("Keyboard")]
    public KeyCode keyboardForward; //thrust
    public KeyCode keyboardBackward;
    public KeyCode keyboardPitchUp; //pitch
    public KeyCode keyboardPitchDown;
    public KeyCode keyboardYawLeft; //yaw
    public KeyCode keyboardYawRight;
    public KeyCode keyboardRollRight; //roll
    public KeyCode keyboardRollLeft;
    public KeyCode keyboardFire;

    //mouse
    public KeyCode mouseLeft;
    public KeyCode mouseRight;

    //Joystick
    [Header("Joystick")]
    public string axisThrust;
    public string axisPitch;
    public string axisYaw;
    public string axisRoll;
    public KeyCode axisFire;

    //TODO joystick


    /* make a keypress act as a float having positive and negative keys that will cancell out each other
 * if both buttons are held down the result will be 0
 * -1 to 1 
 */
    static float WeightButtons(KeyCode positive, KeyCode negative)
    {
        return (Input.GetKey(positive) ? 1f : 0f) - (Input.GetKey(negative) ? 1f : 0f);
    }

    public void UpdateMouseAndKeyBoardActions()
    {
        thrust = WeightButtons(keyboardForward, keyboardBackward);
        pitch = WeightButtons(keyboardPitchUp, keyboardPitchDown);
        roll = WeightButtons(keyboardRollRight, keyboardRollLeft);
        yaw = WeightButtons(keyboardYawRight, keyboardYawLeft);
        fire = Input.GetKey(keyboardFire);
       
    }

    public void debugLog()
    {
        Debug.Log("Fire = "+fire);
        Debug.Log(Input.GetButton("fire1"));
    }

    public void Update()
    {
        switch (type)
        {
            case InputType.MouseAndKeyboard:
                UpdateMouseAndKeyBoardActions();
                break;
            case InputType.Controller:
                UpdateController();
                break;
        }
    }

    public void UpdateController()
    {
        thrust = Input.GetAxis(axisThrust);
        pitch = Input.GetAxis(axisPitch);
        roll = - Input.GetAxis(axisRoll);
        yaw = Input.GetAxis(axisYaw);
        fire = Input.GetKey(axisFire);
    }
}