﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{

	public String nextScene;

	public void ChangeScene()
	{
		SceneManager.LoadScene(nextScene);
	}
}
