﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.PlayerLoop;

public class EventRepeater : MonoBehaviour
{
	public float cooldownTime;
	private bool hasCooldown;

	public UnityEvent TriggeredEvent;
	
	void Update()
	{
		if (!hasCooldown)
		{
			hasCooldown = true;
			StartCoroutine(Cooldown(() =>
			{
				hasCooldown = false;TriggeredEvent.Invoke();},cooldownTime));
		}
	}
	public IEnumerator Cooldown(System.Action condition, float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		condition();
	}
}
