﻿using UnityEngine;

public class InstantiateOnAwake : MonoBehaviour
{
    public bool asChild;
    public GameObject[] objects;

    private void Awake()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            if (asChild)
            {
                Instantiate(objects[i], transform);
            }
            else
            {
                Instantiate(objects[i]);
            }
        }
    }
}
